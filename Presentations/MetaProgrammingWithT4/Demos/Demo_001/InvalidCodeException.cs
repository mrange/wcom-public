﻿// ----------------------------------------------------------------------------------------------
// Copyright (c) WCOM AB.
// ----------------------------------------------------------------------------------------------
// This source code is subject to terms and conditions of the Microsoft Public License. A 
// copy of the license can be found in the License.html file at the root of this distribution. 
// If you cannot locate the  Microsoft Public License, please send an email to 
// dlr@microsoft.com. By using this source code in any fashion, you are agreeing to be bound 
//  by the terms of the Microsoft Public License.
// ----------------------------------------------------------------------------------------------
// You must not remove this notice, or any other, from this software.
// ----------------------------------------------------------------------------------------------

using System;
using System.Globalization;
using System.Runtime.Serialization;
using System.Security;

namespace Demo_001
{
    /// <summary>
    /// Represents InvalidCode errors that occur during application execution.
    /// </summary>
    [Serializable]
    class InvalidCodeException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Demo_001.InvalidCodeException"/> class.
        /// </summary>
        public InvalidCodeException ()
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Demo_001.InvalidCodeException"/> class with a specified error message.
        /// </summary>
        /// <param name="message">The message that describes the error. </param>
        public InvalidCodeException (string message)
            :   base (message)
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Demo_001.InvalidCodeException"/> class with a specified error message and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner exception is specified. </param>
        /// <param name="message">The error message that explains the reason for the exception. </param>
        public InvalidCodeException (Exception innerException, string message)
            :   base (message, innerException)
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Demo_001.InvalidCodeException"/> class with a formatted error message.
        /// </summary>
        /// <param name="format">The error message format that explains the reason for the exception. </param>
        /// <param name="args">The arguments to be formatted. </param>
        public InvalidCodeException (string format, params object[] args)
            :   base (string.Format(CultureInfo.InvariantCulture, format ?? "", args ?? new object[0]))
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Demo_001.InvalidCodeException"/> class with a formatted error message and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner exception is specified. </param>
        /// <param name="format">The error message format that explains the reason for the exception. </param>
        /// <param name="args">The arguments to be formatted. </param>
        public InvalidCodeException (Exception innerException, string format, params object[] args)
            :   base (string.Format(CultureInfo.InvariantCulture, format ?? "", args ?? new object[0]), innerException)
        {
            
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Demo_001.InvalidCodeException"/> class with serialized data.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo"/> that holds the serialized object data about the exception being thrown. </param>
        /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext"/> that contains contextual information about the source or destination. </param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="info"/> parameter is null. </exception>
        /// <exception cref="T:System.Runtime.Serialization.SerializationException">The class name is null or <see cref="P:Demo_001.InvalidCodeException.HResult"/> is zero (0). </exception>
        [SecuritySafeCritical]
        protected InvalidCodeException(SerializationInfo info, StreamingContext context)
        {
        }
    }
}